﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Open_Closed_Principle
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee[] emp = new Employee[10];
            int i = 0;
            Boolean flag = true;
            while (flag == true)
            {
                Console.WriteLine("Press 1 To Add Permanent Employee");
                Console.WriteLine("Press 2 to add Temporary Employee");
                Console.WriteLine("Press 3 To Show Data Of All Employees");
                Console.WriteLine("Press 4 to Exit");
                Console.WriteLine("Enter Your Choice :");
                int a = Convert.ToInt32(Console.ReadLine());
                if (a == 1)
                {
                    Console.WriteLine("Enter ID");
                    int id = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter Name");
                    string name = Console.ReadLine();
                    Console.WriteLine("Enter Salary");
                    double salary = Convert.ToDouble(Console.ReadLine()); 
                    emp[i] = new PermanentEmployee(id,name,salary);
                }
                else if (a == 2)
                {
                    Console.WriteLine("Enter ID");
                    int id = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter Name");
                    string name = Console.ReadLine();
                    Console.WriteLine("Enter Salary");
                    double salary = Convert.ToDouble(Console.ReadLine());
                    emp[i] = new TemporaryEmployee(id, name, salary);

                }
                else if (a == 3)
                {
                    for (int j = 0;j < i;j++)
                    {
                        emp[j].Show();
                        Console.WriteLine("Salary With Bonus : " +  emp[j].calculateBonus());
                        Console.WriteLine("--------------------------------");
                    }
                }
                else if (a == 4)
                {
                    flag = false;
                }
                i = i + 1;
            }

        }
    }
}
