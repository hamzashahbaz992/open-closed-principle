﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Open_Closed_Principle
{
    public abstract class Employee
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public double Salary { get; set; }
        public Employee()
        {

        }
        public Employee (int id,string name,double salary)
        {
            this.ID = id;
            this.Name = name;
            this.Salary = salary;
        }


        public void Show()
        {
            Console.WriteLine("ID : " + this.ID);
            Console.WriteLine("Name : " + this.Name);
            Console.WriteLine("Salary : " + this.Salary);
        }
        public abstract double calculateBonus();
    }
    public class PermanentEmployee : Employee
    {
        public PermanentEmployee()
        {

        }
        public PermanentEmployee(int id,string name,double salary):base(id,name,salary)
        {

        }
        public override double calculateBonus()
        {
            return (Salary * 0.1) + Salary;
        }
    }
    public class TemporaryEmployee : Employee
    {
        public TemporaryEmployee()
        {

        }
        public TemporaryEmployee(int id, string name, double salary)
            : base(id, name, salary)
        {

        }
        public override double calculateBonus()
        {
            return (Salary * 0.05) + Salary;
        }
    }


}
